const screen = document.querySelector('.screen');
const stepsLeft = document.querySelectorAll('.left');
const stepsRight = document.querySelectorAll('.right');

let stepDetected = '';

let negativeReaction = ['url("img/keepRight.png")']

// ARRAY WITH ALL POSSIBLE GIFS FOR POSITIVE REACTIONS
let positiveReaction = ['url("https://c.tenor.com/7Ypq9_9najcAAAAd/thumbs-up-double-thumbs-up.gif")',
                'url("https://c.tenor.com/SNIM3SI-mtIAAAAC/kummeli-thumbs-up.gif")',
                'url("https://c.tenor.com/uFV671ovOhgAAAAC/spongebob-many.gif")',
                'url("https://c.tenor.com/yvYyh_aS9esAAAAC/grilla-yes.gif")',
                'url("https://c.tenor.com/iLK-fRF4uEEAAAAd/batman-thumbs-up.gif")',
                'url("https://c.tenor.com/XpM54W9iO2kAAAAd/thumbs-up-okay.gif")',
                'url("https://c.tenor.com/GBBVrq9U3uUAAAAC/bh187-mr-bean.gif")',
                'url("https://c.tenor.com/eHCXj_dwNeUAAAAC/thumbs-up-horse.gif")',
                'url("https://c.tenor.com/PgfvhIRWfrAAAAAd/jim-carrey-yes-sir.gif")',
                'url("https://c.tenor.com/J5A9wZzn3ZYAAAAd/robert-redford-jeremiah-johnson.gif")'
]

// DETECTS WHETHER CURSOR IS HOVERING OVER ELEMENTS WITH .LEFT CLASS, IF THIS IS THE CASE IT CHANGES THE VALUE OF STEPDETECTED AND FIRES THE screenChange(); FUNCTION
for (let i = 0 ; i < stepsLeft.length; i++) {
    stepsLeft[i].addEventListener('mouseover', function(e) {
        // console.log(e.target);
        stepDetected = 'left';
        // console.log(stepDetected);
        screenChange();
    })
};

// DETECTS WHETHER CURSOR IS HOVERING OVER ELEMENTS WITH .RIGHT CLASS, IF THIS IS THE CASE IT CHANGES THE VALUE OF STEPDETECTED AND FIRES THE screenChange(); FUNCTION
for (let i = 0 ; i < stepsRight.length; i++) {
    stepsRight[i].addEventListener('mouseover', function(e) {
        // console.log(e.target);
        previousValue = stepDetected;
        // console.log(stepDetected);
        stepDetected = 'right';
        if (stepDetected != previousValue) {
            screenChange();
        }
    })
};

// SEVERAL LINES OF CODE, PLACED INSIDE A FUNCTION FOR OPTIMIZATION.
// IT FIRES AN IF STATEMENT OVER THE stepDetected VALUE, IF RIGHT THEN IT TAKES A RANDOMIZED ENTRY FROM THE POSITIVE REACTIONS ARRAY. OTHERWISE THE NEGATIVE ONE
function screenChange() {
    if (stepDetected === 'right') {
        screen.style.backgroundColor = '#cbf95c';
        screen.style.backgroundImage = positiveReaction[Math.floor(Math.random() * 10)];
    }
    else {
        screen.style.backgroundColor = '#f95c5c';
        screen.style.backgroundImage = negativeReaction;
    }
};