# Launch week web prototype

This contains the prototype that I've made for the launch week empathic building project.

Our idea was to create a staircase that 'nudges' people to follow the rules presented on the staircase, such as keeping on the right side. By presenting an animated gif, the user is rewarded for following the correct behaviour.
